# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=ginkgo
pkgver=1.16.3
pkgrel=0
pkgdesc="BDD-style Go testing framework"
url="https://onsi.github.io/ginkgo/"
license="MIT"
arch="all !armhf !mips64" # test failures
options="chmod-clean"
makedepends="go"
source="https://github.com/onsi/ginkgo/archive/v$pkgver/ginkgo-$pkgver.tar.gz
	pie-and-race-conflict.patch
	"

export GOPATH="$srcdir"

build() {
	go build -v -ldflags "-s -w" -o bin/ginkgo ./ginkgo
}

check() {
	go test ./...
}

package() {
	install -Dm755 bin/ginkgo "$pkgdir"/usr/bin/ginkgo
}

cleanup_srcdir() {
	go clean -modcache
	default_cleanup_srcdir
}

sha512sums="
2dc99523687fdbbf46f3e42d7593f85ec2223a055b1ee728879df7cdd6fa7447c8ff6c6aca30016721d8516890d936dc9b243d111d3cbd2ed0c21ad16458e6aa  ginkgo-1.16.3.tar.gz
6d8a1ad1160ccede8eeda204277bb974b9a678efaa5624ab03d8eb204202c26c60e37b3aadd6e6368ac2d57cd073720f9faf4e397bcebb50bc643f680ca45534  pie-and-race-conflict.patch
"
